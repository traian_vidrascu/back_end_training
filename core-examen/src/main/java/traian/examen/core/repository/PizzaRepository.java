package traian.examen.core.repository;

import traian.examen.core.model.Pizza;

/**
 * Created by traian on 16.06.2017.
 */
public interface PizzaRepository extends BaseRepository<Pizza,Long> {
}
