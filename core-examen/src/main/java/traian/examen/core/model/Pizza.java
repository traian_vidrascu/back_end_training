package traian.examen.core.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by traian on 16.06.2017.
 */
@Entity
@Table(name = "pizza")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Pizza extends BaseEntity<Long>{

    @Column(name = "name",unique = true, nullable = false)
    String name;

    @Column(name = "description", nullable = false)
    String description;

    @Column(name = "price",nullable = false)
    Float price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pizza pizza = (Pizza) o;

        return name.equals(pizza.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
