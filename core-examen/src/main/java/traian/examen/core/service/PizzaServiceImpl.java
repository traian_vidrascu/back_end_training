package traian.examen.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import traian.examen.core.model.Pizza;
import traian.examen.core.repository.PizzaRepository;

import java.util.List;


/**
 * Created by traian on 16.06.2017.
 */
@Service
public class PizzaServiceImpl implements PizzaService {

    private static final Logger log = LoggerFactory.getLogger(PizzaServiceImpl.class);

    @Autowired
    private PizzaRepository pizzaRepository;

    @Override
    @Transactional
    public Pizza createPizza(String name, String description, Float price) {
        log.trace("createPizza: name={}, description={}, price={}",name,description,price);
        Pizza pizza = Pizza.builder()
                            .name(name)
                            .description(description)
                            .price(price)
                            .build();
        pizzaRepository.save(pizza);
        log.trace("createPizza: pizza={}",pizza);
        return pizza;
    }

    @Override
    public List<Pizza> findAll() {
        log.trace("findAll - entred");
        List<Pizza> pizzas = pizzaRepository.findAll();
        log.trace("finaAll: pizzas={}",pizzas);
        return pizzas;
    }
}
