package traian.examen.core.service;

import traian.examen.core.model.Pizza;

import java.util.List;

/**
 * Created by traian on 16.06.2017.
 */
public interface PizzaService {

    Pizza createPizza(String name,String description,Float price);

    List<Pizza> findAll();
}
