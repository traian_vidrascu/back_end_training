import { Component, OnInit } from '@angular/core';
import {PizzasService} from "../../services/pizzas.service";
import {Location} from '@angular/common'

@Component({
  selector: 'app-pizza-new',
  templateUrl: './pizza-new.component.html',
  styleUrls: ['./pizza-new.component.css']
})
export class PizzaNewComponent implements OnInit {

  constructor(private location: Location, private pizzasService: PizzasService) { }

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
  }


  save(name, description, price): void {
    if (!this.isValid(name, description, price)) {
      console.log("all fields are required ");
      alert("all fields are required; price has to be an int");
      return;
    }
    this.pizzasService.create(name, description, price)
      .subscribe(_ => this.goBack());
  }

  private isValid(name, description, price) {
    if (!name || !description || !price) {
      console.log("all fields are required");
      return false;
    }
  }
}
