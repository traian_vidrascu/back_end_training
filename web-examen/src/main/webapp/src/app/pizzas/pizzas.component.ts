import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {Pizza} from "../models/pizza";
import {PizzasService} from '../services/pizzas.service'

@Component({
  selector: 'app-pizzas',
  templateUrl: './pizzas.component.html',
  styleUrls: ['./pizzas.component.css']
})
export class PizzasComponent implements OnInit {
  errorMessage: string
  pizzas: Pizza[]
  constructor(private pizzasService :PizzasService, private router :Router) { }

  ngOnInit() {
    this.getPizzas()
  }

  getPizzas(){
    this.pizzasService.getPizzas()
      .subscribe(pizzas => this.pizzas = pizzas,
                 error => this.errorMessage = <any>error);
  }
}
