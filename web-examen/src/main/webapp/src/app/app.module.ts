import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {PizzasService} from  './services/pizzas.service'
import { AppComponent } from './app.component';
import { PizzasComponent } from './pizzas/pizzas.component';
import { PizzaNewComponent } from './pizzas/pizza-new/pizza-new.component';

@NgModule({
  declarations: [
    AppComponent,
    PizzasComponent,
    PizzaNewComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [PizzasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
