package traian.examen.web.dto;

import lombok.*;

import java.io.Serializable;

/**
 * Created by traian on 16.06.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class BaseDto implements Serializable {
    private Long id;
}
