package traian.examen.web.dto;

import lombok.*;

import java.util.List;

/**
 * Created by traian on 16.06.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class PizzasDto{
    private List<PizzaDto> pizzas;
}
