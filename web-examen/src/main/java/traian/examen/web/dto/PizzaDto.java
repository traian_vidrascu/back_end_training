package traian.examen.web.dto;

import lombok.*;

/**
 * Created by traian on 16.06.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PizzaDto extends BaseDto {

    private String name;

    private String description;

    private Float price;

    @Override
    public String toString() {
        return "PizzaDto{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}
