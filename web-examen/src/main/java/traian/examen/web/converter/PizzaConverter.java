package traian.examen.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import traian.examen.core.model.Pizza;
import traian.examen.web.dto.PizzaDto;

/**
 * Created by traian on 16.06.2017.
 */
@Component
public class PizzaConverter extends BaseConverter<Pizza,PizzaDto> {
    private static final Logger log = LoggerFactory.getLogger(PizzaConverter.class);

    @Override
    public PizzaDto convertModelToDto(Pizza pizza){

        PizzaDto pizzaDto = PizzaDto.builder()
                                    .name(pizza.getName())
                                    .description(pizza.getDescription())
                                    .price(pizza.getPrice())
                                    .build();

        pizzaDto.setId(pizza.getId());

        return pizzaDto;
    }
}
