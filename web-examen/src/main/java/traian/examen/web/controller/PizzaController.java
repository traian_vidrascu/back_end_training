package traian.examen.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import traian.examen.core.model.Pizza;
import traian.examen.core.service.PizzaService;
import traian.examen.web.converter.PizzaConverter;
import traian.examen.web.dto.PizzaDto;
import traian.examen.web.dto.PizzasDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by traian on 16.06.2017.
 */
@RestController
@ComponentScan({"traian.examen.core.service"})
public class PizzaController {

    private static final Logger log = LoggerFactory.getLogger(PizzaController.class);

    @Autowired
    PizzaService pizzaService;

    @Autowired
    PizzaConverter pizzaConverter;

    @RequestMapping(value = "/pizzas",method = RequestMethod.POST)
    public Map<String,PizzaDto> createPizza(
            @RequestBody final Map<String,PizzaDto> pizzaDtoMap){
        log.trace("createPizza: pizzaDtoMap={}",pizzaDtoMap);
        PizzaDto pizzaDto = pizzaDtoMap.get("pizza");

        Pizza pizza = pizzaService.createPizza(pizzaDto.getName(),pizzaDto.getDescription(),
                                                pizzaDto.getPrice());
        Map<String,PizzaDto> result = new HashMap<>();
        result.put("pizza",pizzaConverter.convertModelToDto(pizza));

        log.trace("createPizza: result={}",result);
        return result;
    }

    @RequestMapping(value = "/pizzas",method = RequestMethod.GET)
    public PizzasDto getPizzas(){
        log.trace("getPizzas - entred");

        List<Pizza> pizzas = pizzaService.findAll();

        log.trace("getPizzas: pizzas={}",pizzas);
        return new PizzasDto(pizzaConverter.convertModelsToDtos(pizzas));

    }
}
